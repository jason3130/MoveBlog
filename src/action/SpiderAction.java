
package action;

import beans.BlogLink;
import com.google.gson.Gson;
import common.AuthenticationTool;
import common.JsonMsg;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import spider.BlogPageProcessor;
import spider.BlogPipeline;
import spider.LinksList;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.processor.PageProcessor;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 爬虫调用action
 * @author oscfox
 *
 */

@WebServlet("/action/spider")
public class SpiderAction extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String result="";
		String url = request.getParameter("url");

		if(StringUtils.isBlank(url)){
			JsonMsg.json_out(JsonMsg.jsonError("请输入url!"),response);
			return;
		}

		if(!url.contains("http://") && !url.contains("https://")){
			url="http://"+url;
		}

		// 查询授权信息
		Pair<String, String> userToken = AuthenticationTool.ME.getTokenFromCookie(request.getCookies());
		if (userToken == null) {
			JsonMsg.json_out(JsonMsg.jsonError("请先授权!"), response);
			return;
		}

		String user = userToken.getLeft();

		PageProcessor pageProcessor;
		try {
			pageProcessor = new BlogPageProcessor(url);
		} catch (Exception e) {
			e.printStackTrace();
			JsonMsg.json_out(JsonMsg.jsonError("暂不支持该博客网站!"),response);
			return;
		}
		LinksList.clearList(user);
		//爬取博客，结果存放在BLogList中
        Spider.create(pageProcessor)
        	.addUrl(url)
             .addPipeline(new BlogPipeline(user)).run();

        List<BlogLink> linkList=LinksList.getLinkList(user);
        if(null == linkList){
        	JsonMsg.json_out(JsonMsg.jsonError("链接有误或抓取超时！"), response);
        }
        result = new Gson().toJson(linkList);
        JsonMsg.json_out(result, response);
	}
}
